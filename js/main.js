function c(p) {
	console.log(p);
}

function checkScrollPlugin() {
	if ( $(window).width() >= 768 ) {
		if ( jQuery.mCustomScrollbar ) {
			$('.scroll-block').mCustomScrollbar('update');
			$('.scroll-block').css('height', '150px');
			$('.scroll-block').mCustomScrollbar({
				axis: 'y',
				theme: 'scroll-theme'
			});
		}
	} else {
		if ( jQuery.mCustomScrollbar ) {
			$('.scroll-block').mCustomScrollbar('disable');
			$('.scroll-block').css('height', 'auto');
		}
	}
}

$(function() {
	$(".left-nav .parent > span").click(function() {
		if (!$(this).parent().hasClass("active"))
			$(this).parent().addClass("active");
		else
			$(this).parent().removeClass("active");
		$(this).parent().find("ul:first").stop(true, true).toggle(300);
	});
	$('.left-nav .parent > span > a').on('click', function(e) {
		e.preventDefault();
	})
	if ( $('.datepicker-container').length )
		$('.datepicker-container').datepicker();

	if ( $('input[type="file"]').length )
		$('input[type="file"]').nicefileinput();

	if ( $('.scroll-block').length )
		checkScrollPlugin();

	$(window).resize(function() {
		checkScrollPlugin();
	});

	$('.button-hide').on('click', function() {
		var
			_this = $(this),
			collapsable = _this.closest('.collapsable')
		;
		if ( collapsable.hasClass('collapsed') ) {
			collapsable.find('.collapsing-content').stop(true, true).fadeIn();
			collapsable.removeClass('collapsed');
			_this.text('-');
		}
		else {
			collapsable.find('.collapsing-content').stop(true, true).fadeOut();
			collapsable.toggleClass('collapsed');
			_this.text('+');
		}
	})
});